'use strict';

angular.module('indigoApp',['ui.router', 'ngResource', 'ngAnimate'])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider, $sceProvider) {
        $locationProvider.hashPrefix('');

        $stateProvider
        // route for the home page
            .state('app', {
                url:'/',
                views: {
                    'svg': {
                        templateUrl: 'templates/sprite.svg'
                    },
                    'menu': {
                        templateUrl: 'templates/menu.html',
                        resolve: {
                            contactsService: 'contactsService',
                            phonesRes: function (contactsService) {return contactsService.getPhones().query().$promise;},
                            menuService: 'menuService',
                            menuRes: function (menuService) {return menuService.getMenu().query().$promise;},
                            categoriesService: 'categoriesService',
                            categoriesRes: function (categoriesService) {return categoriesService.getCategories().query().$promise;}
                        },
                        controller: 'MenuController'
                    },
                    'content': {
                        templateUrl: 'templates/home.html',
                        resolve: {
                            contactsService: 'contactsService',
                            phonesRes: function (contactsService) {return contactsService.getPhones().query().$promise;},
                            sliderService: 'sliderService',
                            sliderRes: function (sliderService) {return sliderService.getSlides().query().$promise;},
                            categoriesService: 'categoriesService',
                            categoriesRes: function (categoriesService) {return categoriesService.getCategories().query().$promise;}
                        },
                        controller: 'HomeController'
                    },
                    'footer': {
                        templateUrl: 'templates/footer.html',
                        resolve: {
                            contactsService: 'contactsService',
                            phonesRes: function (contactsService) {return contactsService.getPhones().query().$promise;},
                            addressRes: function (contactsService) {return contactsService.getAddress().query().$promise;}
                        },
                        controller: 'FooterController'
                    }
                }
            })

            .state('app.categoryDetail', {
                url: 'categories/:name',
                params: {
                    id: null
                },
                views: {
                    'content@': {
                        templateUrl: 'templates/categories-detail.html',
                        resolve: {
                            categoriesService: 'categoriesService',
                            categoryRes: function (categoriesService, $stateParams) {
                                if ($stateParams.id !== null) {
                                    localStorage.counter = $stateParams.id;
                                }

                                if (localStorage.counter) {
                                    return categoriesService.getCategories()
                                        .get({id:parseInt(localStorage.getItem('counter'),10)}).$promise
                                } else {
                                    return categoriesService.getCategories()
                                        .get({id:parseInt($stateParams.id,10)}).$promise;
                                }

                            }
                        },
                        controller: 'CategoryDetailController'
                    }
                }
            })

            .state('app.contacts', {
                url: 'contacts',
                views: {
                    'content@': {
                        templateUrl: 'templates/contacts.html',
                        resolve: {
                            contactsService: 'contactsService',
                            phonesRes: function (contactsService) {return contactsService.getPhones().query().$promise;},
                            addressRes: function (contactsService) {return contactsService.getAddress().query().$promise;},
                            personRes: function (contactsService) {return contactsService.getPersons().query().$promise;}
                        },
                        controller: 'ContactsController'
                    }
                }
            })

            .state('app.gallery', {
                url: 'gallery',
                views: {
                    'content@': {
                        templateUrl: 'templates/gallery.html',
                        resolve: {
                            galleryService: 'galleryService',

                            galleryPhotoRes: function (galleryService) {
                                return galleryService.getGalleryPhotos().query().$promise;
                            },
                            galleryVideoRes: function (galleryService) {
                                return galleryService.getGalleryVideo().query().$promise;
                            }

                        },
                        controller: 'GalleryController'
                    }
                }
            });



        $urlRouterProvider.otherwise('/');

        $sceProvider.enabled(false);
    })
;
