'use strict';

angular.module('indigoApp')
    .constant('baseURL', 'http://localhost:3000/')



    .service('menuService', ['$resource', 'baseURL', function ($resource, baseURL) {

        this.getMenu = function () {
            return $resource(baseURL + 'menu', null, {'update':{method:'PUT'}});
        };

    }])



    .service('contactsService', ['$resource', 'baseURL', function ($resource, baseURL) {

        this.getPhones = function () {
            return $resource(baseURL + 'phones', null, {'update':{method:'PUT'}});
        };
        this.getAddress = function () {
            return $resource(baseURL + 'address', null, {'update':{method:'PUT'}});
        };
        this.getPersons = function () {
            return $resource(baseURL + 'persons', null, {'update':{method:'PUT'}});
        };

    }])



    .service('categoriesService', ['$resource', 'baseURL', function ($resource, baseURL) {

        this.getCategories = function () {
            return $resource(baseURL + 'categories/:id', null, {'update': {method:'PUT'}});
        };

    }])



    .service('sliderService', ['$resource', 'baseURL', function ($resource, baseURL) {

        this.getSlides = function () {
            return $resource(baseURL + 'slider', null, {'update': {method:'PUT'}});
        };

    }])



    .service('galleryService', ['$resource', 'baseURL', function ($resource, baseURL) {

        this.getGalleryPhotos = function () {
            return $resource(baseURL + 'gallery', null, {'update': {method:'PUT'}});
        };
        this.getGalleryVideo = function () {
            return $resource(baseURL + 'video', null, {'update': {method:'PUT'}});
        };

    }])
;