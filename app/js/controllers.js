'use strict';

angular.module('indigoApp')

    .controller('IndexController', ['$scope', function ($scope) {
    }])

    .controller('HomeController', ['$scope', '$timeout', 'phonesRes', 'sliderRes', 'categoriesRes',
        function ($scope, $timeout, phonesRes, sliderRes, categoriesRes) {

        /** Phones **/
        $scope.phonesList = phonesRes;

        /** Slider **/
        $scope.message = 'Loading data...';
        $scope.showSlider = false;
        $scope.slides = sliderRes;
        $scope.showSlider = true;

        /** Categories **/
        $scope.showCategories = false;
        $scope.categories = categoriesRes;
        $scope.showCategories = true;
    }])
    .directive('owlCarousel', function () {
        return {
            restrict: 'E',
            link: function (scope) {
                scope.initCarousel = function (e) {
                    var navTextLeft = `
                        <svg class="icon-arrow">
                            <use class="default" xlink:href="#big_arrow"></use>
                            <use class="hover" xlink:href="#big_arrow-hover"></use>
                        </svg>`;
                    var navTextRight = `
                        <svg class="icon-arrow">
                            <use xlink:href="#big_arrow"></use>
                            <use class="hover" xlink:href="#big_arrow-hover"></use>
                        </svg>`;
                    var owlOptions = {
                        items: 1,
                        loop: true,
                        dots: true,
                        nav: true,
                        navText: [navTextLeft, navTextRight],
                        autoplay: true,
                        autoplayHoverPause: true
                    };
                    $(e).owlCarousel(owlOptions);
                }
            }
        }
    })
    .directive('owlCarouselItem', function () {
        return {
            restrict: 'A',
            link: function (scope, e) {
                if(scope.$last) {
                    scope.initCarousel(e.parent());
                }
            }
        }
    })

    .controller('MenuController', ['$scope', 'menuRes', 'phonesRes', 'categoriesRes',
        function ($scope, menuRes, phonesRes, categoriesRes) {

        /** Menu **/
        $scope.showMenu = false;
        $scope.message = 'Loading data...';
        /* getting data */
        $scope.menuList = menuRes;
        $scope.showMenu = true;
        addDropdown();
        /* dropdown */
        function addDropdown() {
            setTimeout(function () {
                $('.js_dropdown').on('click', function (e) {
                    e.preventDefault();
                    $('.js_second-lvl').slideToggle(300);
                    $('.icon-dropdown').toggleClass('rotated')
                });
            }, 200);
        }

        /** Phones **/
        $scope.showPhones = false;
        $scope.phonesList = phonesRes;
        $scope.showPhones = true;

        /** Categories **/
        $scope.showCategories = false;
        $scope.categories = categoriesRes;
        $scope.showCategories = true;

        /** Menu Dropdown **/
        $scope.dropdownVisible = false;
        $scope.changeDropdown = function (e) {
            if (e !== 'Услуги') {
                return $scope.dropdownVisible = !$scope.dropdownVisible;
            }
        }
    }])

    .controller('FooterController', ['$scope', 'phonesRes', 'addressRes', function ($scope, phonesRes, addressRes) {

        /** Phones **/
        $scope.showPhones = false;
        $scope.phonesList = phonesRes;
        $scope.showPhones = true;

        /** Address **/
        $scope.showAddress = false;
        $scope.addressList = addressRes;
        $scope.showAddress = true;

    }])

    .controller('GalleryController', ['$scope', '$sce', 'galleryPhotoRes', 'galleryVideoRes',
        function ($scope, $sce, galleryPhotoRes, galleryVideoRes) {

        /** Gallery photos **/
        $scope.showGalleryPhotos = false;
        $scope.photosList = galleryPhotoRes;
        $scope.showGalleryPhotos = true;

        /** Gallery Video **/
        $scope.showGalleryVideo = false;
        $scope.videoList = galleryVideoRes;
        $scope.showGalleryVideo = true;

        $scope.test = function () {
            console.log('ANOTHER TEEEST');
        }
    }])
    // .directive('masonry', function () {
    //     return {
    //         restrict: 'A',
    //         link: function (scope) {
    //             scope.initMasonry = function (e) {
    //                 var masonryOptions = {
    //                     itemSelector: '.gallery__photos-item',
    //                     columnWidth: '.masonry-sizer',
    //                     percentPosition: true,
    //                     gutter: 30
    //                 };
    //                 $(e).masonry(masonryOptions);
    //             }
    //         }
    //     }
    // })
    // .directive('masonryElem', function () {
    //     return {
    //         restrict: 'A',
    //         link: function (scope, e) {
    //             if(scope.$last) {
    //                     scope.initMasonry(e.parent());
    //             }
    //         }
    //     }
    // })
    .directive('imageOnLoad', function() {
        return {
            restrict: 'A',
            link: function(scope, element) {
                element.bind('load', function() {
                    if (scope.$last) {
                        $('.gallery__photos').masonry({
                            itemSelector: '.gallery__photos-item',
                            columnWidth: '.masonry-sizer',
                            percentPosition: true,
                            gutter: 30
                        });
                    }
                });
                element.bind('error', function(){
                    alert('image could not be loaded');
                });
            }
        };
    })



    .controller('ContactsController', ['$scope', 'phonesRes', 'addressRes', 'personRes',
        function ($scope, phonesRes, addressRes, personRes) {

        /** Phones **/
        $scope.message = 'Loading data...';
        $scope.showPhones = false;
        $scope.phonesList = phonesRes;
        $scope.showPhones = true;

        /** Address **/
        $scope.showAddress = false;
        $scope.addressList = addressRes;
        $scope.showAddress = true;

        /** Persons **/
        $scope.showPersons = false;
        $scope.personsList = personRes;
        $scope.showPersons = true;

    }])

    .controller('CategoryDetailController', ['$scope', '$stateParams', 'categoryRes',
                            function ($scope, $stateParams, categoryRes) {

        $scope.category = categoryRes;

    }])
;