/** CSS **/
var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');        //adding vendor prefixes
var csso = require('gulp-csso');                        //creating min.css files
var sass = require('gulp-sass');                        //making css from scss

/** JS **/
var babel = require('gulp-babel');                      //making ES5 from ES6
var uglify = require('gulp-uglify');                    //creating min.js files
var ngannotate = require('gulp-ng-annotate');

/** Other **/
var concat = require('gulp-concat');                    //combining few files to one
var sourcemaps = require('gulp-sourcemaps');            //sourcemaps generating
var rename = require('gulp-rename');


var path = {
    app: {
        css: {
            main: 'app/css/',
            libs: 'app/css/libs'
        },
        js: {
            all: 'app/js/**/*.js',
            libs:'app/js/libs/'
        }
    },
    dist: {
        css: 'dist/css/',
        js: 'dist/js/',
        main: 'dist/',
        img: 'dist/img/',
        fonts: 'dist/fonts/'
    },
    public: {
        css: 'json-server/public/css/',
        js: 'json-server/public/js/',
        main: 'json-server/public/',
        img: 'json-server/public/img/',
        fonts: 'json-server/public/fonts/'
    },
    libs: {
        bootstrap: {
            scss: 'bower_components/bootstrap-sass/assets/stylesheets/**/*.scss'
        },
        jquery: 'bower_components/jquery/dist/jquery.min.js',
        angular: {
            main: 'bower_components/angular/angular.min.js',
            uiRouter: 'bower_components/angular-ui-router/release/angular-ui-router.min.js',
            resource: 'bower_components/angular-resource/angular-resource.min.js',
            animate: 'bower_components/angular-animate/angular-animate.min.js'
        },
        owlCar: {
            js: 'bower_components/owl.carousel/dist/owl.carousel.min.js',
            css: 'bower_components/owl.carousel/dist/assets/owl.carousel.min.css',
            theme: 'bower_components/owl.carousel/dist/assets/owl.theme.default.min.css'
        },
        masonry: 'bower_components/masonry/dist/masonry.pkgd.min.js',
        fancyBox: {
            js: 'bower_components/fancybox/dist/jquery.fancybox.min.js',
            css: 'bower_components/fancybox/dist/jquery.fancybox.min.css'
        }
    }
};

function errorLog(error) {
    console.log(error);
    this.emit('end');
}

gulp.task('css', function () {
    gulp.src('app/css/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .on('error', errorLog)
        .pipe(autoprefixer({ browsers: ['defaults', 'ie >= 9', 'last 15 versions'] }))
        .pipe(csso())
        .pipe(concat({path: 'bundle.min.css', cwd: ''}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.dist.css))
        .pipe(gulp.dest(path.public.css))
});

gulp.task('js', function () {
    gulp.src('app/js/*.js')
        .pipe(sourcemaps.init())
        .pipe(babel({ presets: ['es2015'] }))
        .pipe(ngannotate())
        .pipe(concat({path: 'main.min.js', cwd: ''}))
        .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.dist.js))
        .pipe(gulp.dest(path.public.js))
});

gulp.task('watch', ['all'], function () {
    gulp.watch('app/css/**/*.*', ['css']);
    gulp.watch('app/js/*.js', ['js']);
    gulp.watch('app/js/libs/*.*', ['use:jslibs']);
    gulp.watch('app/**/*.{html,svg}', ['copy:html']);
    gulp.watch('app/img/**/*.*', ['copy:img']);
    gulp.watch('app/fonts/**/*.*', ['copy:fonts'])
});

gulp.task('use:bootstrap', function () {
    return gulp.src(path.libs.bootstrap.scss)
        .pipe(gulp.dest(path.app.css.libs))
});
gulp.task('use:jquery', function () {
    return gulp.src(path.libs.jquery)
        .pipe(gulp.dest(path.app.js.libs))
});
gulp.task('use:owlCar', function () {
    gulp.src(path.libs.owlCar.js)
        .pipe(gulp.dest(path.app.js.libs));
    gulp.src([path.libs.owlCar.css, path.libs.owlCar.theme])
        .pipe(gulp.dest(path.app.css.libs))
});
gulp.task('use:angular', function () {
    return gulp.src([path.libs.angular.main, path.libs.angular.uiRouter, path.libs.angular.resource, path.libs.angular.animate])
        .pipe(concat({path: 'angular-full.min.js', cwd: ''}))
        .pipe(gulp.dest(path.app.js.libs))
});
gulp.task('use:masonry', function () {
    return gulp.src(path.libs.masonry)
        .pipe(gulp.dest(path.app.js.libs))
});
gulp.task('use:fancybox', function () {
    gulp.src(path.libs.fancyBox.js)
        .pipe(rename('z.fancybox.min.js'))
        .pipe(gulp.dest(path.app.js.libs));
    gulp.src(path.libs.fancyBox.css)
        .pipe(gulp.dest(path.app.css.libs))
});




gulp.task('copy:html', function () {
    gulp.src('app/**/*.{html,svg}')
        .pipe(gulp.dest(path.dist.main))
        .pipe(gulp.dest(path.public.main))
});

gulp.task('copy:img', function () {
    gulp.src('app/img/**/*.*')
        .pipe(gulp.dest(path.dist.img))
        .pipe(gulp.dest(path.public.img))
});

gulp.task('copy:fonts', function () {
    gulp.src('app/fonts/**/*.*')
        .pipe(gulp.dest(path.dist.fonts))
        .pipe(gulp.dest(path.public.fonts))
});

gulp.task('use:jslibs', ['use:all'], function () {
    gulp.src('app/js/libs/*.js')
        .pipe(ngannotate())
        .pipe(concat({path: 'lib.min.js', cwd: ''}))
        .pipe(gulp.dest(path.dist.js))
        .pipe(gulp.dest(path.public.js))
});

gulp.task('use:all', ['use:bootstrap', 'use:jquery', 'use:owlCar', 'use:angular', 'use:masonry', 'use:fancybox']);
gulp.task('startCss', ['use:all'], function () {
    gulp.src('app/css/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .on('error', errorLog)
        .pipe(autoprefixer({ browsers: ['defaults', 'ie >= 9', 'last 15 versions'] }))
        .pipe(csso())
        .pipe(concat({path: 'bundle.min.css', cwd: ''}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.dist.css))
        .pipe(gulp.dest(path.public.css))
});

gulp.task('all', ['use:all', 'startCss', 'js', 'use:jslibs', 'copy:html', 'copy:img', 'copy:fonts']);